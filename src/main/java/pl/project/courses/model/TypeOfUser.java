package pl.project.courses.model;

public enum TypeOfUser {
    ADMIN(true, true, true),
    LECTURER(true, true, false),
    STUDENT(true, false, false);

    boolean reads;
    boolean sends;
    boolean accepts;

    TypeOfUser(boolean ifReads, boolean ifSends, boolean ifAccepts) {
        reads = ifReads;
        sends = ifSends;
        accepts = ifAccepts;
    }
}
